﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(jenkins_test.Startup))]
namespace jenkins_test
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
